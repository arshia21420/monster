from django.urls import path
from .views import account_details, CarUserAdd, CarUserUpdate, CarUserDelete, Profile, Login
from django.contrib.auth.views import (
    LoginView,
    LogoutView,
    PasswordChangeDoneView
)


app_name = 'account'


urlpatterns = [
    path('', account_details.as_view(), name='admin'),
    path('car/create', CarUserAdd.as_view(), name='car_create'),
    path('car/update/<int:pk>', CarUserUpdate.as_view(), name='car_update'),
    path('car/Delete/<int:pk>', CarUserDelete.as_view(), name='car_delete'),
    path('profile/', Profile.as_view(), name='profile'),
]