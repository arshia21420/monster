from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (ListView, CreateView, UpdateView, DeleteView, )
from base.models import car_model
from django.urls import reverse_lazy
from django.contrib.auth import get_user_model
from .mixins import (
FieldMixin, FormValidMixin,
Author_accessMixin, SuperUser_Access_Mixin
)
from .models import User
from .form import ProfileForm
from django.contrib.auth.views import LoginView, PasswordChangeView
# Create your views here.
class account_details(LoginRequiredMixin ,ListView):
    queryset = car_model.objects.all()
    template_name = 'registration/home.html'

    def get_queryset(self):
        if self.request.user.is_superuser:
            return car_model.objects.all()
        else:
            return car_model.objects.filter(author=self.request.user)


class CarUserAdd(LoginRequiredMixin , FieldMixin, FormValidMixin, CreateView):
    model = car_model
    template_name = 'registration/car-create-update.html'


class CarUserUpdate(Author_accessMixin , FieldMixin, FormValidMixin, UpdateView):
    model = car_model
    template_name = 'registration/car-create-update.html'


class CarUserDelete(LoginRequiredMixin, SuperUser_Access_Mixin,DeleteView):
    model = car_model
    success_url = reverse_lazy('home')
    template_name = 'registration/car_delete.html'


class Profile(LoginRequiredMixin, UpdateView):
    model = User
    template_name = 'registration/profile.html'
    success_url = reverse_lazy('account:profile')
    form_class = ProfileForm

    def get_object(self):
        return User.objects.get(pk=self.request.user.pk)


    def get_form_kwargs(self):
        kwargs = super(Profile, self).get_form_kwargs()
        kwargs.update({
            'user': self.request.user
        })
        return kwargs


class Login(LoginView):
    def get_success_url(self):
        user = self.request.user

        if user.is_superuser or user.is_author:
            return reverse_lazy('account:admin')
        else:
            return reverse_lazy('account:profile')






from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from .form import SignupForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.core.mail import EmailMessage

class Register(CreateView):
    form_class = SignupForm
    template_name = 'registration/register.html'

    def form_valid(self, form):
        if self.request.method == 'POST':
            form = SignupForm(self.request.POST)
            if form.is_valid():
                user = form.save(commit=False)
                user.is_active = False
                user.save()
                current_site = get_current_site(self.request)
                mail_subject = 'Activate your blog account.'
                message = render_to_string('registration/activateAcc.html', {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': force_str(urlsafe_base64_encode(force_bytes(user.pk))),
                    'token': account_activation_token.make_token(user),
                })
                to_email = form.cleaned_data.get('email')
                email = EmailMessage(
                    mail_subject, message, to=[to_email]
                )
                email.send()
                return HttpResponse('Please confirm your email address to complete the registration')



def activate(request, uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        # return redirect('home')
        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')
