from django.http import Http404
from django.shortcuts import get_object_or_404
from base.models import car_model
class FieldMixin():
    def dispatch(self, request, *args, **kwargs):
        self.fields = [
            'Category', 'title', 'production_name', 'slug',
            'description', 'image', 'publish', 'is_special', 'status', 'color', 'year'
        ]
        if request.user.is_superuser:
            self.fields.append("author")
        return super().dispatch(request, *args, **kwargs)


class Author_accessMixin():
    def dispatch(self, request, pk,*args, **kwargs):
            # car_obj = get_object_or_404(car_model, pk=pk)
            if request.user.is_authenticated:
                if car_model.author == request.user and car_model.status in ['b', 'd'] or request.user.is_superuser:
                    return super().dispatch(request, *args, **kwargs)
                else:
                    raise Http404('You Dont Have Access')
            else:
                    raise Http404('You Have to login first !!!')

class FormValidMixin():
    def form_valid(self, form):
        if self.request.user.is_superuser:
            form.save()
        else:
            self.obj = form.save(commit=False)
            self.obj.author = self.request.user
            if not self.obj.status in ['d' , 'i']:
                self.obj.status = 'd'
        return super().form_valid(form)

class SuperUser_Access_Mixin():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404('You Dont Have Access')
