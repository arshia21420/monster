from .models import User
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
# Register your models here.
UserAdmin.fieldsets += (
('فیلد های من', {'fields': ('is_author', 'special_user')}),
)

UserAdmin.list_display += ('is_author', 'is_special_user')
admin.site.register(User, UserAdmin)