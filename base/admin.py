from django.contrib import admin
from .models import car_model, Category
from base.models import IPAddress
from account.models import User
# Register your models here.
@admin.action(description='انتشار محصولات انتخاب شده')
def make_published(modeladmin, request, queryset):
    queryset.update(status='p')


@admin.action(description='پیشنویس محصولات انتخاب شده')
def make_draft(modeladmin, request, queryset):
    queryset.update(status='d')


class Car_admin(admin.ModelAdmin):
    list_display = ('image_tag', 'title', 'author', 'production_name', 'publish', 'status', 'color', 'year', 'category_to_str', 'is_special')
    list_filter = ('title', 'year')
    search_fields = ('title', 'year')
    ordering = ('year', )
    actions = [make_published, make_draft]


    def category_to_str(self, obj):
        return ", ".join([Category.title for Category in obj.Category.all()])
admin.site.register(car_model,Car_admin)


class Category_admin(admin.ModelAdmin):
    list_display = ('title', 'status', 'position', 'slug')
    list_filter = ('title', 'position')
    search_fields = ('title', 'status')


admin.site.register(Category, Category_admin)
admin.site.register(IPAddress)

