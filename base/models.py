from django.db import models
from account.models import User
from django.utils import timezone
from django.utils.html import format_html
from django.urls import reverse
from django.contrib.contenttypes.fields import GenericRelation
from comment.models import Comment
#Managers
class CarManager(models.Manager):
    def published(self):
        return self.filter(status='p')


# Create your models here.

class IPAddress(models.Model):
    ip_address = models.GenericIPAddressField(verbose_name='آدرس آی پی')



class Category(models.Model):
    cat_parent = models.ForeignKey('self', default=None, null=True, blank=True, on_delete=models.SET_NULL, related_name='cat_children', verbose_name='زیردسته')
    title = models.CharField(max_length=200, verbose_name='مدل')
    slug = models.SlugField(max_length=100, unique=True)
    status = models.BooleanField(default=True, verbose_name='آیا نمایش داده شود؟')
    position = models.IntegerField(unique=True, verbose_name='کد')


    class Meta():
        verbose_name = 'دسته بندی'
        verbose_name_plural = ' دسته بندی ها'
        ordering = ['position']
    def __str__(self):
        return self.title


class car_model(models.Model):
    STATUS_CHOICES = (
        ('d' , 'Draft'),
        ('p' , 'Published'),
        ('i' , 'locked'),
        ('b' , 'rejected'),

    )
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name='car_author', verbose_name='فروشنده')
    Category = models.ManyToManyField(Category, verbose_name='دسته بندی')
    title = models.CharField(max_length=200, verbose_name='نام')
    production_name = models.CharField(max_length=30,verbose_name='شرکت')
    slug = models.SlugField(max_length=100, unique=True)
    description = models.TextField(verbose_name='مشخصات')
    image = models.ImageField(upload_to='Car_model_images', verbose_name='تصویر')
    publish = models.DateTimeField(default=timezone.now, verbose_name='تاریخ')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, verbose_name='انتشار')
    color = models.CharField(max_length=30, verbose_name='رنگ')
    year = models.IntegerField()
    is_special = models.BooleanField(default=False, verbose_name='اشتراک ویژه')
    comments = GenericRelation(Comment)
    hits = models.ManyToManyField(IPAddress, through='carViewHits', blank=True, related_name='hits', verbose_name='بازدید ها')
    def get_absolute_url(self):
        return reverse('account:admin')


    def __str__(self):
        return self.title


    class Meta():
        verbose_name = 'بوگاتی'
        verbose_name_plural = 'بوگاتی ها'

    def image_tag(self):
        return format_html("<img width=100 style='border-radius:5px;' src='{}'>".format(self.image.url))



class carViewHits(models.Model):
    car_hit = models.ForeignKey(car_model, on_delete=models.CASCADE)
    ip_address = models.ForeignKey(IPAddress, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)