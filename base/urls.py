from django.urls import path
from base import views

urlpatterns = [
    path('', views.home, name='home'),
    path('product', views.ProductList.as_view(), name='products'),
    path('contactUs', views.ContactUs, name='contact'),
    path('product_detail/<slug:slug>', views.ProductDetailList.as_view(), name='product_detail_page'),
    path('search/', views.searchProduct.as_view(), name='searchbar'),
    path('search/page/<int:page>', views.searchProduct.as_view(), name='searchbar')
]




