from django.shortcuts import render
from django.views.generic import ListView, DetailView
from .models import Category, car_model
from django.shortcuts import get_object_or_404
from account.models import User
from django.core.paginator import Paginator
from django.db.models import Count
from base.models import car_model
from django.db.models import Q
# Create your views here.
def home(request):
    context = {
        'category' : Category.objects.filter(status=True)
    }
    return render(request, 'Website/home.html', context)

class ProductList(ListView):
    queryset = car_model.objects.filter(status='p').order_by('-publish').annotate(count=Count('hits')).order_by('-count', '-publish')
    template_name = 'Website/products.html'
    paginate_by = 3




class ProductDetailList(DetailView):
    template_name = 'Website/product_detail_page.html'
    def get_object(self):
        slug = self.kwargs.get('slug')
        carmodel = get_object_or_404(car_model.objects.filter(status='p').order_by('-publish'), slug=slug)
        ip_address = self.request.user.ip_address
        if ip_address not in carmodel.hits.all():
            carmodel.hits.add(ip_address)
        return carmodel


class searchProduct(ListView):
    paginate_by = 20
    template_name = 'product_searchs/searchbar.html'

    def get_queryset(self):
        search = self.request.GET.get('q')
        return car_model.objects.filter(Q(description__icontains=search) |Q(title__icontains=search))
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search'] = self.request.GET.get('q')
        return context

def ContactUs(request):
    return render(request, 'Website/contact.html', {})